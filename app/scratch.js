/**
 * Created by clint on 11/15/15.
 */

var ref = new Firebase("https://<YOUR-FIREBASE-APP>.firebaseio.com");
$scope.authObj = $firebaseAuth(ref);

$scope.authObj.$authWithPassword({
    email: "my@email.com",
    password: "mypassword"
}).then(function(authData) {
    console.log("Logged in as:", authData.uid);
}).catch(function(error) {
    console.error("Authentication failed:", error);
});