/**
 * Created by clint on 8/19/15.
 */
(function () {
    "use strict";

    angular
        .module("productManagement")
        .controller("PriceAnalyticsCtrl",
                    ["products",
                        "productService",
                        "$filter",
                        PriceAnalyticsCtrl]);

    function PriceAnalyticsCtrl(products, productService, $filter){
        var vm = this;
        vm.title = "Price Analytics";

        var titleTrimLength = 10;
        for(var i = 0; i < products.length; i++){
            products[i].marginPercent =
                productService.calculateMarginPercent(products[i].price,
                                                        products[i].cost);
            products[i].marginAmount =
                productService.calculateMarginAmount(products[i].price,
                                                        products[i].cost);
        }

        var orderedProductsAmount = $filter("orderBy")(products, "marginAmount");
        var filteredProductAmount = $filter("limitTo")(orderedProductsAmount,5);

        var chartLabelAmount = [];
        var chartCostAmount = [];
        var chartPriceAmount = [];
        var chartMarginAmount = [];
        for(var i = 0; i < filteredProductAmount.length; i++){

            if(filteredProductAmount[i].productName.length > titleTrimLength){
                var trimmedTitle = filteredProductAmount[i].productName.substring(0,titleTrimLength) + "...";
                chartLabelAmount.push(trimmedTitle);
            }
            else{
                chartLabelAmount.push(filteredProductAmount[i].productName);
            }

            chartCostAmount.push(filteredProductAmount[i].cost);
            chartPriceAmount.push(filteredProductAmount[i].price);
            chartMarginAmount.push(filteredProductAmount[i].marginAmount);
        }
        vm.chartAllAmount = [chartCostAmount,chartPriceAmount,chartMarginAmount];
        vm.labelsAmount = chartLabelAmount;
        vm.seriesAmount = ["Cost","Price","Margin"];


        var orderedProductsPercent = $filter("orderBy")(products, "marginPercent");
        var filteredProductsPercent = $filter("limitTo")(orderedProductsPercent,5);

        var chartLabelPercent = [];
        var chartDataPercent = [];

        for(var i = 0; i < filteredProductsPercent.length; i++){

            if(filteredProductsPercent[i].productName.length > titleTrimLength){
                var trimmedTitle = filteredProductsPercent[i].productName.substring(0,titleTrimLength) + "...";
                chartLabelPercent.push(trimmedTitle);
            }
            else{
                chartLabelPercent.push(filteredProductsPercent[i].productName);
            }

            chartDataPercent.push(filteredProductsPercent[i].marginPercent);
        }

        vm.labelsPercent = chartLabelPercent;
        vm.dataPercent = [
            chartDataPercent
        ];
        vm.seriesPercent = ["% Margin"];
    }
}());