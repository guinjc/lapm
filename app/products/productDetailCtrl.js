/**
 * Created by clint on 8/14/15.
 */
(function () {
    "use strict";

    angular
        .module("productManagement")
        .controller("ProductDetailCtrl",
                    ["productService",
                        "product",
                        ProductDetailCtrl]);

    function ProductDetailCtrl(productService,product){
        var vm = this;
        vm.product = product;

        vm.marginPercent = productService.calculateMarginPercent(vm.product.price,vm.product.cost);
        if(vm.product.tags){
            vm.product.tagList = vm.product.tags.toString();
        }
    }

}());