/**
 * Created by Clint on 8/13/2015.
 */
(function () {
    "use strict";
    angular
        .module("productManagement")
        .controller("ProductListCtrl",["products","$filter","$mdDialog","productResource", ProductListCtrl]);

    function ProductListCtrl(products, $filter,$mdDialog, productResource) {
        var vm = this;

        vm.products = products;

        vm.showImage = true;

        vm.toggleImage = function () {
            vm.showImage = !vm.showImage;
        }

        var orderBy = $filter('orderBy');

        vm.order = function (predicate, reverse) {
            vm.products = orderBy(vm.products, predicate, reverse)
        }

        vm.showConfirm = function(ev, id) {
            // Appending dialog to document.body to cover sidenav in docs app

            var product = productResource.get(id).$loaded()
                .then(function(p) {

                    var confirm = $mdDialog.confirm()
                    .title('Would you like to delete item ' + p.productName +"?")
                    .content('This action is not reversable.')
                    .ariaLabel('Delete product')
                    .ok('Continue')
                    .cancel('Cancel')
                    .targetEvent(ev);
                    $mdDialog.show(confirm).then(function() {
                        p.$remove();
                    }, function() {

                    });
                })
                .catch(function(error) {
                    console.error("Error:", error);
                });;


        };
    }

}());
