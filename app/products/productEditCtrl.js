/**
 * Created by clint on 8/17/15.
 */
(function () {
    "use strict";

    angular
        .module("productManagement")
        .controller("ProductEditCtrl",
        ["product",
            "$state",
            "productService",
            "productResource",
            ProductEditCtrl]);

    function ProductEditCtrl(product, $state, productService, productResource) {
        var vm = this;

        vm.product = product;

        vm.priceOption = "percent";

        if (vm.product.releaseDate)
            vm.tempDate = new Date(vm.product.releaseDate);
        vm.marginPercent = function () {
            return productService.calculateMarginPercent(vm.product.price, vm.product.cost);
        }

        vm.calculatePrice = function () {

            var price = 0;

            if (vm.priceOption == 'amount') {
                price = productService.calculatePriceFromMarkupAmount(
                    vm.product.cost, vm.markupAmount);

            }

            if (vm.priceOption == 'percent') {
                price = productService.calculatePriceFromMarkupPercent(
                    vm.product.cost, vm.markupPercent);

            }
            vm.product.price = price;
        }
        if (vm.product.$id != 0) {
            vm.title = "Edit: " + vm.product.productName;

        }
        else {
            vm.title = "New Product";
        }

        vm.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            vm.opened = !vm.opened;
        }

        vm.submit = function (isValid) {
            if (isValid) {
                if (!vm.product.price)
                    vm.product.price = 0;
                if (!vm.product.cost)
                    vm.product.cost = 0;

                if (!vm.product.imageUrl)
                    vm.product.imageUrl = "https://openclipart.org/download/189154/no-camera-sign.svg";

                if (vm.tempDate)
                    vm.product.releaseDate = vm.tempDate.toDateString();
                if (vm.product.$id == 0) {
                    var products = productResource.query();
                    products.$add(vm.product).then(function (ref) {

                        vm.product = productResource.get({productId: ref.key()});
                        toastr.success("Save Successful");

                    }).catch(function (error) {

                        alert('Error!');

                    });
                } else {
                    vm.product.$save().then(function () {
                        toastr.success("Save Successful");
                    }).catch(function (error) {
                        alert('Error!');
                    });
                }
            }

            else
                toastr.error("Please fix validation issues");
        }

        vm.cancel = function () {
            $state.go('productList');
        }


        vm.addTags = function (tags) {

            if (tags) {
                var tagsToAdd = tags.split(',');

                if (vm.product.tags) {
                    for (var i = 0; i < tagsToAdd.length; i++) {
                        var index = vm.product.tags.indexOf(tagsToAdd[i]);

                        if (index == -1) {
                            vm.product.tags.push(tagsToAdd[i]);
                        }
                    }
                }
                else {
                    vm.product.tags = tagsToAdd;
                }

                vm.newTags = "";
            } else {
                alert("Please enter one or more tags separated by commas");
            }
        }

        vm.removeTag = function (tag) {
            vm.product.tags.splice(vm.product.tags.indexOf(tag), 1);
        }

    }

}());