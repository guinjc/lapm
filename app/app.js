/**
 * Created by Clint on 8/13/2015.
 */

(function () {
    "use strict";
    var app = angular.module("productManagement",
        ["common.services", "ui.router", "ui.mask", "ui.bootstrap", "chart.js", "ngMaterial", "ngMessages", "users", "firebase"]);


    app.config(function ($provide, $mdThemingProvider) {
        $provide.decorator("$exceptionHandler",
            ["$delegate",
                function ($delegate){
                    return function (exception, cause){
                        exception.message = "Please contact the Help Desk! \nMessage: " +
                                exception.message;

                        $delegate(exception, cause);
                        alert(exception.message);
                    };
                }]);

        $mdThemingProvider.definePalette('docs-blue', $mdThemingProvider.extendPalette('blue', {
            '50': '#DCEFFF',
            '100': '#AAD1F9',
            '200': '#7BB8F5',
            '300': '#4C9EF1',
            '400': '#1C85ED',
            '500': '#106CC8',
            '600': '#0159A2',
            '700': '#025EE9',
            '800': '#014AB6',
            '900': '#013583',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': '50 100 200 A100',
            'contrastStrongLightColors': '300 400 A200 A400'
        }));

        $mdThemingProvider.definePalette('docs-red', $mdThemingProvider.extendPalette('red', {
            'A100': '#DE3641'
        }));

        $mdThemingProvider.theme('docs-dark', 'default')
            .primaryPalette('yellow')
            .dark();

        $mdThemingProvider.theme('default')
            .primaryPalette('docs-blue')
            .accentPalette('docs-red');
    });


    app.config(["$stateProvider",
            "$urlRouterProvider",
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise("/");

                $stateProvider

                    .state("home", {
                        url: "/",
                        templateUrl: "app/welcomeView.html",
                        controller: "UserController as vm"
                    })

                    .state("productList", {
                        url: "/products",
                        templateUrl: "app/products/productListView.html",
                        controller: "ProductListCtrl as vm",
                        resolve : {
                            productResource: "productResource",

                            products: function (productResource) {
                                return productResource.query().$loaded();
                            }
                        }
                    })

                    .state("productEdit", {
                        abstract: true,
                        url: "/products/edit/:productId",
                        templateUrl: "app/products/productEditView.html",
                        controller: "ProductEditCtrl as vm",
                        resolve: {
                            productResource: "productResource",

                            product: function (productResource, $stateParams) {
                                var productId = $stateParams.productId;
                                return productResource.get({productId:productId});
                            }
                        }
                    })
                    .state("productEdit.info",{
                        url: "/info",
                        templateUrl: "app/products/productEditInfoView.html"
                    })
                    .state("productEdit.price",{
                        url:"/price",
                        templateUrl: "app/products/productEditPriceView.html"
                    })
                    .state("productEdit.tags",{
                        url:"/tags",
                        templateUrl:"app/products/productEditTagsView.html"
                    })
                    .state("productEdit.image",{
                        url:"/image",
                        templateUrl:"app/products/productEditImageView.html"
                    })
                    .state("productDetail",{
                        url: "/products/:productId",
                        templateUrl: "app/products/productDetailView.html",
                        controller: "ProductDetailCtrl as vm",
                        resolve : {
                            productResource: "productResource",

                            product: function (productResource, $stateParams) {
                                var productId = $stateParams.productId;
                                return productResource.get({productId:productId}).$loaded();
                            }
                        }
                    })
                    .state("priceAnalytics",{
                        url:"/priceAnalytics",
                        templateUrl:"app/prices/priceAnalyticsView.html",
                        controller: "PriceAnalyticsCtrl as vm",
                        resolve : {
                            productResource: "productResource",

                            products: function (productResource) {
                                return productResource.query().$loaded();
                            }
                        }
                    }
                )

            }]
    );
}());