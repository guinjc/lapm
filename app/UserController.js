(function () {

    angular
        .module('productManagement')
        .controller('UserController', [
            '$mdSidenav', "$firebaseAuth", '$mdBottomSheet', '$scope', '$timeout', '$state','$rootScope',
            UserController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function UserController($mdSidenav, $firebaseAuth, $mdBottomSheet, $scope, $timeout,$state,$rootScope) {
        var vm = this;

        vm.loggedin = false;

        vm.toggleSideNav = toggleSideNav;
        vm.login = login;
        vm.register = register;
        vm.loginFaceBook = loginFaceBook;

        vm.logout = logout;
        var ref = new Firebase("https://lapm.firebaseio.com");
        // *********************************
        // Internal methods
        // *********************************

        ref.onAuth(function(authData) {
            if (authData ) {
                console.log("Authenticated with uid:", authData.uid);
                $state.go('productList');
                vm.loggedin = true;
            } else {
                console.log("Client unauthenticated.");
                $state.go('home');
                vm.loggedin = false;
            }
        });

        $scope.$on('LoggedIn', function() {
            vm.loggedin = true;
            $state.go('home');
            $scope.$apply();

        });

        //$scope.$on('LoggedOut', function() {
        //    vm.loggedin = false;
        //    $state.go('home');
        //});

        /**
         * First hide the bottomsheet IF visible, then
         * hide or Show the 'left' sideNav area
         */
        function toggleSideNav() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function () {
                $mdSidenav('left').toggle();
            });
        }
        function login() {
            ref.authWithPassword({
                email: vm.email,
                password: vm.password
            }, function (error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                } else {
                    console.log("Authenticated successfully with payload:", authData);
                    $scope.$emit('LoggedIn');
                }
            });
        }

        function logout() {
            ref.unauth();
            $timeout(function() {
                $scope.$emit('LoggedOut');
            }, 500);

        }

        function register() {
            ref.createUser({
                email: vm.email,
                password: vm.password
            }, function (error, userData) {
                if (error) {
                    console.log("Error creating user:", error);
                } else {
                    console.log("Successfully created user account with uid:", userData.uid);
                    vm.loggedin = true;
                    login();
                }
            });
        }

        function loginFaceBook(){
            ref.authWithOAuthPopup("facebook", function(error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                } else {
                    console.log("Authenticated successfully with payload:", authData);
                    $scope.$emit('LoggedIn');
                }
            });
        }


    }

})();
