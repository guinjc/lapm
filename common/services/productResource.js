/**
 * Created by Clint on 8/13/2015.
 */
(function(){
    "use strict";

    angular
        .module("common.services")
        .factory("productResource", ["$firebaseArray", "$firebaseObject", productResource]);

    //old way with $resource
/*    function productResource($resource){
        return $resource("/api/products/:productId")
    }*/

    function productResource($firebaseArray, $firebaseObject){

        var url = 'https://lapm.firebaseio.com/products';
        var ref = new Firebase(url);
        var array = $firebaseArray(ref);

        var query = function(){
            return array;
        }

        var get = function(p){
            var productRef = ref.child(p.productId);
            return $firebaseObject(productRef);
        }

        var add = function(p){
            array.$add(p);
        }

        return{
            query: query,
            get: get,
            add: add
        };
    }

}());